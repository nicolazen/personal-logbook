from typing import Callable

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user
from peewee import fn
from werkzeug.security import check_password_hash, generate_password_hash

from logbook.models import User

auth = Blueprint(  # pylint: disable=invalid-name
    "auth", __name__, template_folder="../templates", static_folder="../static"
)


@auth.route("/login")
def login() -> str:
    return render_template("login.html")


@auth.route("/login", methods=["POST"])
def login_post() -> Callable:
    username = str(request.form.get("username"))
    password = str(request.form.get("password"))
    remember = bool(request.form.get("remember"))

    user = User.get_or_none(fn.Lower(User.username) == username.lower())

    # inform the user if the username/password is wrong
    if user is None or not check_password_hash(user.password, password):
        flash("Please check your login details and try again.")
        return redirect(url_for("logbook.index"))

    login_user(user, remember=remember)
    return redirect(url_for("logbook.index"))


@auth.route("/signup")
def signup() -> str:
    return render_template("signup.html")


@auth.route("/signup", methods=["POST"])
def signup_post() -> Callable:

    username = str(request.form.get("username")).lower()
    password = str(request.form.get("password"))

    user = User.get_or_none(
        fn.Lower(User.username) == username
    )  # if this returns a user, then the email already exists in database

    if user is not None:
        flash("Username already exists")
        return redirect(url_for("auth.signup"))

    # create new user with the form data. Hash the password so plaintext version isn't saved.
    new_user = User(
        username=username, password=generate_password_hash(password, method="sha256")
    )
    new_user.save()

    return redirect(url_for("auth.login"))


@auth.route("/logout", methods=["POST"])
@login_required
def logout() -> Callable:
    logout_user()
    return redirect(url_for("auth.login"))
