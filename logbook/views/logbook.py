from typing import Callable, Dict, List

import humanize

import markdown2
from flask import Blueprint, redirect, render_template, request, url_for
from flask_login import login_required
from logbook.models import LogItem

logbook = Blueprint(  # pylint: disable=invalid-name
    "logbook", __name__, template_folder="../templates", static_folder="../static"
)


@logbook.route("/", defaults={"page_nr": 1})
@logbook.route("/<page_nr>")
@login_required
def index(page_nr):
    items = get_logs(page_nr)
    return render_template("index.html", items=items, page_nr=int(page_nr))


@logbook.route("/logs/<log_id>/edit", methods=["GET"])
@login_required
def get_log_entry(log_id):
    content = get_log_content(log_id)
    return render_template("edit_log.html", content=content, log_id=int(log_id))


@logbook.route("/logs/new", methods=["GET"])
@login_required
def create_new_log():
    return render_template("new_log_form.html")


@logbook.route("/logs", methods=["POST"])
@login_required
def store_log() -> Callable:
    item = LogItem(content=request.form["text"])
    item.save()
    return redirect(url_for("logbook.index"))


def get_logs(page: int = 1) -> List[Dict]:
    items = list(LogItem.select().order_by(LogItem.id.desc()).paginate(int(page)))
    response = []
    for item in items:
        row = {}
        row["id"] = item.id
        row[
            "timestamp"
        ] = f'{item.timestamp.strftime("%m-%d-%Y %H:%M:%S")} ({humanize.naturaldelta(item.timestamp)} ago)'
        row["content"] = (
            markdown2.markdown(item.content, extras=["fenced-code-blocks"])
            .strip()
            .replace("<img", '<img class="ui image"')
        )
        response.append(row)
    return response


def get_log_content(log_id: int) -> str:
    log = LogItem.get(LogItem.id == log_id)
    return log.content


@logbook.route("/logs/<log_id>/edit", methods=["POST"])
@login_required
def update_log_content(log_id: int) -> Callable:
    new_content = request.form["text"]
    log = LogItem.get(LogItem.id == log_id)
    log.content = new_content
    log.save()
    return redirect(url_for("logbook.index"))


@logbook.route("/logs/<log_id>/delete", methods=["POST"])
@login_required
def delete_log_entry(log_id: int) -> Callable:
    """ Deleting with a POST as forms do not support DELETE and I
       am trying to stay javascript-free where possible"""
    log = LogItem.get(LogItem.id == log_id)
    log.delete_instance()
    return redirect(url_for("logbook.index"))
