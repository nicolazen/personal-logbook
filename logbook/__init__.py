# pylint: disable=invalid-name

import os
from typing import Callable, Union

from flask import Blueprint, Flask, redirect, url_for
from flask_assets import Bundle, Environment
from flask_login import LoginManager
from logbook.views import auth, logbook
from logbook.models import User
from logbook.helpers import get_random_border_style

# create main Flask app
app = Flask(__name__)
app.config.from_pyfile("../settings.cfg")

# Static assets management
assets = Environment(app)

css = Bundle(
    "css/lib/tailwind.css",
    "css/lib/pygments.css",
    "css/lib/unpoly.css",
    filters="rcssmin",
    output="{}/_gen/main.css".format(app.config["BASE_URL"]),
)
assets.register("main_css", css)

js = Bundle(
    "js/helpers.js",
    "js/lib/unpoly.min.js",
    "js/lib/config.js",
    "js/event_listeners.js",
    filters="rjsmin",
    output="{}/_gen/site.js".format(app.config["BASE_URL"]),
)
assets.register("site_js", js)

# Authentication
login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id: int) -> Union[None, int]:
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.get_or_none(User.id == user_id)


# Jinja configuration
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True
app.add_template_global(name="get_random_border_style", f=get_random_border_style)

# Blueprints
app.register_blueprint(logbook, url_prefix="/{}".format(app.config["BASE_URL"]))
app.register_blueprint(auth, url_prefix="/{}".format(app.config["BASE_URL"]))

# Catch all to avoid 404s
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def catch_all(path: str) -> Callable:  # pylint: disable=unused-argument
    """ Send any unmatched route to the login page """
    return redirect(url_for("auth.login"))
