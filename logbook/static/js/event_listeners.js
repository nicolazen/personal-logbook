// Unpoly modals do not respect the autofocus attribute, but emit an event
// when the modal is opened and loaded, hence the focus can be triggered
document.addEventListener("up:modal:opened", event => {
  const el = document.querySelector("textarea[autofocus]");
  el.focus();
  auto_grow(el);
});
