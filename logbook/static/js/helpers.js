// Adjust text area height to fit the content dynamically
function auto_grow(element) {
  element.style.height = "auto";
  element.style.height = element.scrollHeight + "px";
}
