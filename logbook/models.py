# pylint: disable=too-few-public-methods

import datetime

from flask_login import UserMixin
from peewee import CharField, DateTimeField, Model, SqliteDatabase

db = SqliteDatabase("logbook/logbook.db")  # pylint: disable=invalid-name


class LogItem(Model):
    """ Contains the log entries and related metadata """

    timestamp = DateTimeField(default=datetime.datetime.now)
    content = CharField()

    class Meta:  # pylint: disable=missing-class-docstring
        database = db


class User(UserMixin, Model):
    """ Contains the users info """

    username = CharField(unique=True)
    password = CharField()

    class Meta:  # pylint: disable=missing-class-docstring
        database = db


db.connect()
db.create_tables([LogItem, User])
