import sys
import os
import subprocess

from logbook import app

# CLI parameters
DEVELOPMENT_MODE = "dev" in sys.argv
BUILD_ASSETS = "build-assets" in sys.argv
LINT = "lint" in sys.argv
SERVE = "serve" in sys.argv


def __build_assets_task():
    print("Started asset building...")
    env = os.environ
    if not DEVELOPMENT_MODE:  # assume production environment
        env = {**os.environ, "NODE_ENV": "production"}
    subprocess.Popen("npm run build-css", env=env, shell=True).wait()
    print("Assets built")


def __linting_task():
    print("Started linting...")
    print("  > BLACK")
    subprocess.Popen("python -m black logbook", shell=True).wait()
    print("  > MYPY")
    subprocess.Popen(
        "python -m mypy logbook --ignore-missing-imports", shell=True
    ).wait()
    print("  > PYLINT")
    subprocess.Popen("python -m pylint logbook", shell=True).wait()
    print("Linting finished")


def __serve_task():
    print("Started server...")
    app.run("127.0.0.1", port=5000, debug=DEVELOPMENT_MODE)


# put tasks in logical order so that they can be combined at the command line
# without running into issues, like serving before building the assets
if BUILD_ASSETS:
    __build_assets_task()

if LINT:
    __linting_task()

if SERVE:
    __serve_task()
